import Vue from 'vue';
import VueAxios from 'vue-axios';
import App from './App.vue';
import Homepage from './Homepage.vue';
import LostProperty from './LostProperty.vue';
import Charter from "./Charter";
import Schedules from "./Schedules";
import ToggleSwitch from 'vuejs-toggle-switch';
import vueCountryRegionSelect from 'vue-country-region-select';
import VueCookies from 'vue-cookies';

Vue.config.productionTip = false
window.Bus = new Vue();
window.axios = require('axios');

if(document.getElementById("app")){
  console.log('app')
  new Vue({
    render: h => h(App)
  }).$mount('#app');
}

if(document.getElementById("homepage")){
  console.log('homepage')
  new Vue({
    render: h => h(Homepage)
  }).$mount('#homepage');
}

if(document.getElementById("lostproperty")){
  console.log('lostproperty')
  new Vue({
    render: h => h(LostProperty)
  }).$mount('#lostproperty');
}

if(document.getElementById("charter")){
  console.log('charter')
  new Vue({
    render: h => h(Charter)
  }).$mount('#charter');
}

if(document.getElementById("schedules")){
  console.log('schedules')
  new Vue({
    render: h => h(Schedules)
  }).$mount('#schedules');
}

Vue.mixin({
  methods: {
    setLocalStorage(storeItems)
    {
      Object.keys(storeItems).forEach( key => {
        let itemToStore = storeItems[key];

        if( typeof storeItems[key] === 'object')
          itemToStore = JSON.stringify(itemToStore);

        localStorage[key] = itemToStore;
      });
    },
  }
});

Vue.directive('click-outside', {
  bind () {
    this.event = event => this.vm.$emit(this.expression, event)
    this.el.addEventListener('click', this.stopProp)
    document.body.addEventListener('click', this.event)
  },
  unbind() {
    this.el.removeEventListener('click', this.stopProp)
    document.body.removeEventListener('click', this.event)
  },

  stopProp(event) { event.stopPropagation() }
})
Vue.use(ToggleSwitch, VueAxios);
Vue.use(vueCountryRegionSelect);
Vue.use(VueCookies);